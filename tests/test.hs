module Main where

import Test.Tasty
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.HUnit

import Data.List
import Data.Ord

import Data.List.HelpersTest
import Data.Time.DayOfWeekTest
import Grocery.CalendarTest

main = defaultMain allTests

allTests :: TestTree
allTests = testGroup "Tests" [
    Data.List.HelpersTest.tests
  , Data.Time.DayOfWeekTest.tests
  , Grocery.CalendarTest.tests
  ]
