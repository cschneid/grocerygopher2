module Grocery.CalendarTest where

import Test.Tasty
import Test.Tasty.QuickCheck as QC

import Grocery.Calendar
import Grocery.Types.Meal
import Grocery.Types.Recipe
import Grocery.Types.Food
import Data.Time
import Data.Ord
import Data.List (nub, sortBy)
import Control.Applicative
import Test.QuickCheck.Instances
import Control.Monad

instance Arbitrary Food where
  arbitrary = Food <$> arbitrary

instance Arbitrary Recipe where
  arbitrary = Recipe <$> pure Nothing
                     <*> arbitrary
                     <*> (do count <- choose (0,3)
                             replicateM count arbitrary)
instance Arbitrary Meal where
  arbitrary = Meal <$> pure Nothing
                   <*> arbitrary
                   <*> arbitrary
                   <*> (do count <- choose (0,3)
                           replicateM count arbitrary)

tests = testGroup "Grocery.Calendar"
  [ QC.testProperty "No day is repeated" $
      \meals -> map fst (createMealCalendar meals)
                  == nub (map fst (createMealCalendar meals))
  , QC.testProperty "Days are in ascending order" $
      \meals -> let cal = createMealCalendar meals
                in  all (== LT) $
                      zipWith compare
                        (map fst cal)
                        (tail $ map fst cal)
  , QC.testProperty "All days are represented between start & finish" $
      \meals -> let earliestDay = minimum (map _day meals)
                    latestDay   = maximum (map _day meals)
                    daysCovered = fromIntegral $ diffDays latestDay earliestDay
                in  length meals == 0 || (length (createMealCalendar meals)) == (daysCovered + 1)
  , QC.testProperty "All the meals are still there" $
      \meals -> let cal = createMealCalendar meals
                    calMeals = mconcat $ map snd cal
                in  calMeals == sortBy (comparing _day) meals
  ]

