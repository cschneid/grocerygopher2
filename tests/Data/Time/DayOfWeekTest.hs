module Data.Time.DayOfWeekTest where

import Test.Tasty
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.HUnit

import Data.Time.DayOfWeek
import Data.Time
import Test.QuickCheck.Instances

tests :: TestTree
tests = testGroup "Data.Time.DayOfWeek" [ dayOfWeekTests
                                        , beginningOfWeekTests
                                        ]

dayOfWeekTests       = testGroup "dayOfWeek"       [dayOfWeekUnitTests]
beginningOfWeekTests = testGroup "beginningOfWeek" [beginningOfWeekProperties, beginningOfWeekUnitTests]

dayOfWeekUnitTests = testGroup "Unit tests" [
    testCase "January 1 2015 is a Thursday" $ let jan1 = fromGregorian 2015 01 01
                                              in dayOfWeek jan1 @?= Thursday
  ]

beginningOfWeekProperties = testGroup "Properties" [
    QC.testProperty "Always is a Sunday" $
      \day -> dayOfWeek (beginningOfWeek day) == Sunday

  , QC.testProperty "Always is a before the day passed in" $
      \day -> beginningOfWeek day <= day
  ]

beginningOfWeekUnitTests = testGroup "Unit tests" [
  ]

