module Data.List.HelpersTest where

import Test.Tasty
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.HUnit

import Data.List (nub)

import Data.List.Helpers

tests :: TestTree
tests = testGroup "Data.List.Helpers" [ selectTests
                                      , separateTests
                                      ]

selectTests   = testGroup "select"   [selectQCProps, selectUnitTests]
separateTests = testGroup "separate" [separateQCProps, separateUnitTests]

selectQCProps = testGroup "Properties"
    [
      QC.testProperty "length of input == length of output" $
        \list -> length (select (list :: [Int])) == length list

    , QC.testProperty "with a unique input list, all outputs are unique" $
        \list ->
          let uniqList = nub (list :: [Int])
          in  nub (select uniqList) == select uniqList

    , QC.testProperty "each item is picked in order" $
        \list -> map fst (select (list :: [Int])) == list
    ]
selectUnitTests = testGroup "Unit tests" []

separateQCProps = testGroup "Properties"
    [
      QC.testProperty "length of input == length of output" $
        \list -> length (separate (list :: [Int])) == length list

    , QC.testProperty "merging the list back together equals original list" $
        \list -> let merge (xs,y,zs) = xs ++ [y] ++ zs
                     mergedLists = map merge (separate list)
                 in  all (== (list :: [Int])) mergedLists
    ]
separateUnitTests = testGroup "Unit tests" []
