module Main where

data User = User { name :: String, age :: Int } deriving (Show)

getName :: User -> String
getName (User n _) = n

getAge :: User -> Int
getAge (User _ a) = a

lookupUser :: Int -> IO (Maybe User)
lookupUser 1 = return $ Just (User "Chris" 30)
lookupUser 2 = return $ Nothing

main :: IO ()
main = do
  u1 <- lookupUser 1
  print $ show u1
  print $ show $ fmap getName u1

  u2 <- lookupUser 2
  print $ show u2
  print $ show $ fmap getName u2

