module Data.List.Helpers where

-- 13:56 Cale: > select [1,2,3,4]
-- 13:56 lambdabot: [(1,[2,3,4]),(2,[1,3,4]),(3,[1,2,4]),(4,[1,2,3])]
select [] = []
select (x:xs) = (x,xs) : [(y,x:ys) | (y,ys) <- select xs]

-- 13:56 Cale: > separate [1,2,3,4]
-- 13:56 lambdabot: [([],1,[2,3,4]),([1],2,[3,4]),([1,2],3,[4]),([1,2,3],4,[])]
separate [] = []
separate (x:xs) = ([],x,xs) : [(x:us,v,vs) | (us,v,vs) <- separate xs]

