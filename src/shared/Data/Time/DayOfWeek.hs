module Data.Time.DayOfWeek where

import Data.Time
import Data.Time.Calendar.WeekDate

data DayOfWeek = Sunday
               | Monday
               | Tuesday
               | Wednesday
               | Thursday
               | Friday
               | Saturday
               deriving (Eq, Ord, Enum, Show)

dayOfWeek :: Day -> DayOfWeek
dayOfWeek d = toEnum $ (third $ toWeekDate d) `mod` 7

-- Find the previous Sunday to this date unless it is Sunday, then don't change
beginningOfWeek :: Day -> Day
beginningOfWeek d = addDays offset d
  where
  offset = negate . fromIntegral $ if third (toWeekDate d) == 7
                                   then 0
                                   else third $ toWeekDate d

first  (a, _, _) = a
second (_, b, _) = b
third  (_, _, c) = c

