module Data.Aeson.Day (dummy) where

import Data.Aeson
import Data.Time
import GHC.Generics

----
-- Borrowed from http://stackoverflow.com/questions/23438900/haskell-date-parsing
-- Makes Aeson instances for the Data.Time Day type
deriving instance Generic Day
instance ToJSON  Day
instance FromJSON  Day


dummy = undefined
