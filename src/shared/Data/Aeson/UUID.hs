module Data.Aeson.UUID where

import Data.Aeson
import Data.UUID
import Control.Monad

instance ToJSON UUID where
  toJSON = toJSON . toString

instance FromJSON UUID where
  parseJSON (String s) = case fromString (show s) of
                           Just u  -> return u
                           Nothing -> mzero

