module Network.Http.StatusHelpers where

-- Inclusive on both ends
isInRange :: (Ord a) => a -> a -> a -> Bool
isInRange l h x = x >= l && x <= h

isHttpInformational :: (Ord a, Num a) => a -> Bool
isHttpInformational = isInRange 100 199

isHttpSuccess :: (Ord a, Num a) => a -> Bool
isHttpSuccess = isInRange 200 299

isHttpRedirect :: (Ord a, Num a) => a -> Bool
isHttpRedirect = isInRange 300 399

isHttpError :: (Ord a, Num a) => a -> Bool
isHttpError = isInRange 400 499

isHttpServerError :: (Ord a, Num a) => a -> Bool
isHttpServerError = isInRange 500 599

