module Grocery.Calendar where

import Grocery.Types.Meal

import Control.Arrow ((&&&))
import Data.List (sortBy)
import Data.Monoid
import Data.Ord
import Data.Time
import qualified Data.Map.Strict as Map

type MealCalendar = [(Day, [Meal])]

createMealCalendar :: [Meal] -> MealCalendar
createMealCalendar [] = []
createMealCalendar ms = Map.toList $ foldl (\cal day -> Map.insert day [] cal) realValues missingDays
  where
  missingDays = filter
                  (\day -> Map.notMember day realValues)
                  [minimum (map _day ms) .. maximum (map _day ms)]
  realValues = Map.fromListWith (flip (<>)) zipped
  zipped = sortBy (comparing fst) $ map (_day &&& (:[])) ms

