module Grocery.Types.User where

import Control.Lens
import Data.Aeson
import Data.Default
import GHC.Generics
import qualified Data.Text as T

import Grocery.Types.UserPassword

type EmailAddress = T.Text

data User = User { _userEmail    :: EmailAddress
                 , _userPassword :: UserPassword
                 } deriving (Eq, Show, Ord, Generic)

instance ToJSON User
instance FromJSON User

instance Default User where
  def = User "" (UnhashedPassword "")

makeLenses ''User

