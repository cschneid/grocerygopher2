module Grocery.Types.UserPassword where

import GHC.Generics
import Data.ByteString
import Data.Aeson
import qualified Data.Text as T

data UserPassword = HashedPassword (Maybe ByteString) | UnhashedPassword T.Text
                    deriving (Show, Eq, Ord, Generic)

instance ToJSON UserPassword where
  toJSON (HashedPassword _)   = object ["type" .= ("HashedPassword" :: T.Text)]
  toJSON (UnhashedPassword v) = object ["type" .= ("UnhashedPassword" :: T.Text),
                                        "value" .= v]

instance FromJSON UserPassword where
  parseJSON (Object o) = do
    t <- o .: "type"
    if t == ("HashedPassword" :: T.Text)
    then return $ HashedPassword Nothing
    else do
      pass <- o .: ("value" :: T.Text)
      return $ UnhashedPassword pass

