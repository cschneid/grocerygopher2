module Grocery.Types.Meal where

import           GHC.Generics
import           Data.Aeson
import           Data.Aeson.Day
import           Data.Aeson.UUID
import           Data.UUID
import           Data.Time
import           Data.Maybe
import qualified Data.Text as T
import           Control.Lens

import Grocery.Types.Recipe

newtype MealID = MealID { toUUID :: UUID }
                 deriving (Show, Eq, Ord, Generic, ToJSON, FromJSON)

data Meal = Meal { _uuid     :: Maybe MealID
                 , _day      :: Day
                 , _mealName :: T.Text
                 , _mealRecipes :: [Recipe]
                 } deriving (Eq, Show, Ord, Generic)

instance ToJSON Meal
instance FromJSON Meal

makeLenses ''Meal
