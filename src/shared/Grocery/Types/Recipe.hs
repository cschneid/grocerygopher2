module Grocery.Types.Recipe where

import GHC.Generics
import Control.Lens
import Data.Aeson
import Data.Aeson.UUID

import           Data.Time
import           Data.UUID
import qualified Data.Text as T

import Grocery.Types.Food

newtype RecipeID = RecipeID { toUUID :: UUID }
                   deriving (Show, Eq, Ord, Generic, ToJSON, FromJSON)

data Recipe = Recipe { _uuid        :: Maybe RecipeID
                     , _recipeName  :: T.Text
                     , _recipeFoods :: [Food]
                     } deriving (Eq, Show, Ord, Generic)

instance ToJSON Recipe
instance FromJSON Recipe

makeLenses ''Recipe

