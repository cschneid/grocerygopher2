module Grocery.Types.Food where

import GHC.Generics
import Data.Aeson
import Control.Lens
import qualified Data.Text as T

data Food = Food { _foodName :: T.Text
                 } deriving (Eq, Show, Ord, Generic)

instance ToJSON Food
instance FromJSON Food

makeLenses ''Food

