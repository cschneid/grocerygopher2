{-# LANGUAGE TypeFamilies                #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Grocery.Database where

import GHC.Generics
import qualified Data.Text as T
import qualified Data.ByteString as BS
import Data.Time
import Database.Persist as DB
import Database.Persist.TH

-- results <- liftIO $ runDB pool $ DB.selectList [DbUserDbUserEmail DB.==. email] [DB.LimitTo 1]

-- src/server/Login.hs:56:51:
--     Not in scope: data constructor ‘DbUserDbUserEmail’
--     cabal: Error: some packages failed to install:
--     grocery-0.1.0.0 failed during the building phase. The exception was:
--     ExitFailure 1

share [mkPersist sqlSettings { mpsGenerateLenses = True, mpsPrefixFields = False }, mkMigrate "migrateAll" ] [persistLowerCase|
DbUser sql=user
    dbUserEmail T.Text
    dbUserPasswordHash BS.ByteString
    dbUserCreatedAt UTCTime
    deriving Show
    deriving Eq
    deriving Generic

DbMeal sql=meal
    dbMealDay UTCTime
    dbMealName T.Text
    dbMealCreatedAt UTCTime Maybe default=CURRENT_TIME
    deriving Show
    deriving Eq
    deriving Generic

DbRecipe sql=recipe
    dbRecipeName T.Text
    deriving Show
    deriving Eq
    deriving Generic

DbFood sql=food
    dbFoodName T.Text
    deriving Show
    deriving Eq
    deriving Generic
|]

