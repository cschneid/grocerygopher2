module Types where

import qualified Grocery.Types as GT
import qualified Grocery.Types.Meal as GT
import qualified Grocery.Types.Recipe as GT
import qualified Grocery.Types.Food as GT
import qualified Grocery.Types.User as GT

import qualified Login.StateMachine as Login
import qualified Calendar.StateMachine as Calendar
import qualified Grocery.Calendar

import Data.Typeable
import qualified Data.Text as T
import Control.Lens
import Data.UUID

data UIState = LoginScreen
             | RegisterScreen
             | CalendarScreen
             deriving (Eq, Ord, Show, Typeable)

data MealPlanState = MealPlanState {
                       _uiState      :: UIState
                     , _user         :: GT.User
                     , _loginState   :: Login.LoginState
                     , _flashMessage :: T.Text
                     , _meals        :: Grocery.Calendar.MealCalendar
                     } deriving (Eq, Show, Typeable)

makeLenses ''MealPlanState

data UIAction = NullActionA
              | UpdateUsernameA T.Text
              | UpdateUserPasswordA T.Text
              | LoginUserA
              | LogoutUserA
              | LoginResultA Login.LoginAction
              | RegisterUserA
              | ChangeToScreenA UIState
              | UpdateFlashA T.Text
              | MealA GT.MealID Calendar.MealAction
              deriving (Eq, Ord, Show, Typeable)

-- | ActionChannel takes a UIAction, injects it back into the application cycle
--   and returns nothing.  Use this for the IO work to communicate state changes back
--   to the application
type ActionChannel = (UIAction -> IO ())

-- | MealPlanRequest takes a channel to talk back to the application as its
--   first argument and can perform whatever IO it may need to
type MealPlanRequest = [ActionChannel -> IO ()]
