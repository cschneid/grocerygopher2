module Calendar.View where

import Types
import Grocery.Types
import Grocery.Types.Meal
import Grocery.Types.Recipe
import Grocery.Types.Food

import qualified Text.Blaze.Event                     as E
import qualified Text.Blaze.Event.Keycode             as Keycode
import qualified Text.Blaze.Html5                     as H
import qualified Text.Blaze.Html5.Attributes          as A

import Control.Lens
import Data.Foldable (foldMap)
import Data.Time

renderDay :: (Day, [Meal]) -> H.Html UIAction
renderDay (d, ms) = undefined

renderMeal :: Meal -> H.Html UIAction
renderMeal meal = do
  H.h2 $ H.toHtml $ meal ^. mealName
  foldMap renderRecipe (meal ^. mealRecipes)

renderRecipe :: Recipe -> H.Html UIAction
renderRecipe recipe = do
  H.h3 $ H.toHtml $ recipe ^. recipeName
  H.ul $ foldMap renderFood (recipe ^. recipeFoods)

renderFood :: Food -> H.Html UIAction
renderFood food = H.li $ H.toHtml $ food ^. foodName

