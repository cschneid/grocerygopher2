module Calendar.StateMachine where

import qualified Grocery.Types as GT
import qualified Grocery.Types.Meal as GT
import qualified Grocery.Types.Recipe as GT
import qualified Grocery.Types.Food as GT
import Data.Typeable

data MealAction = AddMealA
                | AddRecipeA
                | AddFoodA
                | RemoveRecipeA GT.RecipeID
                deriving (Eq, Show, Ord, Typeable)

-- Take an action and apply it to the meal, yielding an updated version
transition :: MealAction -> GT.Meal -> GT.Meal
transition (AddFoodA) m      = undefined
transition (RemoveRecipeA{}) m = undefined

-- [Meal] -> index?
--           day + index?
--           day + id?
-- 


