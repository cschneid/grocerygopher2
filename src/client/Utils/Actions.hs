module Utils.Actions (
    consoleLog,
    )  where


import Types

import qualified Data.Text as T

consoleLog :: T.Text -> ActionChannel -> IO ()
consoleLog t _ = print t
