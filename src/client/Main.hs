module Main where

import Grocery.Types
import Types

import           Blaze.Core
import           Blaze.ReactJS.Base
import qualified Blaze.ReactJS.Run  as ReactJS

import qualified Text.Blaze.Event                     as E
import qualified Text.Blaze.Event.Keycode             as Keycode
import qualified Text.Blaze.Html5                     as H
import qualified Text.Blaze.Html5.Attributes          as A

import MealPlan as MealPlan
import Control.Concurrent

import Debug.Trace


main :: IO ()
main = ReactJS.runApp' MealPlan.renderState (fmap handle MealPlan.app)

handle :: MealPlanRequest -> ActionChannel -> IO ()
handle req act = do
  if length req > 0
  then (head req) act
  else return ()

