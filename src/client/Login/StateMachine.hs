module Login.StateMachine where

import qualified Data.Text as T
import qualified Grocery.Types.User as GT

type LoginMessage = T.Text

data LoginState =
  -- This user is logged in
    LoggedIn GT.User

  -- The user is not logged in. Show a form and let them do stuff
  -- The message allows for failed logins to display something
  | LoggedOut (Maybe LoginMessage)

  -- We have submitted this user, and are waiting for a response
  -- from the server
  | LoginInProgress GT.User
  deriving (Show, Eq, Ord)

data LoginAction =
    LogoutA
  | SubmitFormA      GT.User
  | ResponseFailureA LoginMessage
  | ResponseSuccessA GT.User
  deriving (Show, Eq, Ord)

transition :: LoginAction -> LoginState -> LoginState
transition  LogoutA             (LoggedIn _)        = LoggedOut Nothing
transition (SubmitFormA u)      (LoggedOut _)       = LoginInProgress u
transition (ResponseFailureA m) (LoginInProgress _) = LoggedOut (Just m)
transition (ResponseSuccessA u) (LoginInProgress _) = LoggedIn u
transition state action =
  error $ "Invalid transition. From " ++ show state ++ " with " ++ show action

