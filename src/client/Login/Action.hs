module Login.Action (
      loginUser
    , logoutUser
    , registerUser
    ) where

import Grocery.Types.User
import Types
import Login.StateMachine
import Network.Http.StatusHelpers

import Control.Lens
import Data.Aeson (toJSON)
import Data.Text as T
import Data.Default
import JavaScript.JQuery

loginUser :: User -> ActionChannel -> IO ()
loginUser user act = do
  act $ LoginResultA (SubmitFormA user)

  (AjaxResult s d) <- makeAjaxCall user
  if isHttpSuccess s
  then act $ LoginResultA (ResponseSuccessA user)
  else act $ LoginResultA (ResponseFailureA "Failed!") -- TODO: Add message
  where 
    url = "/login"
    makeAjaxCall user = ajax url (toJSON user) (def { asMethod = POST })

logoutUser :: ActionChannel -> IO ()
logoutUser act = do
  -- TODO: Is there anything to do in order to log out?
  act $ LoginResultA LogoutA

--------------------------------------------------------------------------------
-- Helpers
--------------------------------------------------------------------------------



registerUser :: User -> ActionChannel -> IO ()
registerUser user act = do
  (AjaxResult s d) <- makeAjaxCall user
  act $ UpdateFlashA $ T.pack . show $ d
  where
    url = "/register"
    makeAjaxCall user = ajax url (toJSON user) (def { asMethod = POST })

