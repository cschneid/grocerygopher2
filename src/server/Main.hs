{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module Main where

import DatabaseSupport
import qualified Login as Login
import Grocery.Types
import Grocery.Database

import Control.Monad.Logger (runStdoutLoggingT)
import Control.Monad.Trans (liftIO)
import Control.Monad (void)
import Data.IORef
import Data.Proxy
import Data.Time
import Data.Aeson
import Data.Monoid
import Control.Lens

import Web.Spock.Simple
import Network.Wai
import Network.Wai.Handler.Warp
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Gzip
import Network.Wai.Middleware.Static
import Network.Socket
import Network.HTTP.Types.Status

import qualified Data.Text as T

import qualified Database.Persist as DB
import qualified Database.Persist.Sqlite as DB

import GHC.Generics

main = do
  pool <- getPool
  runDB pool (DB.runMigration migrateAll)

  mainApp <- spockAsApp $ spock sessionConfig (PCPool pool) appState (routes pool)
  run 7000 $ applyMiddleware $ mainApp

  where
    sessionConfig :: SessionCfg Int
    sessionConfig = SessionCfg {
                        sc_cookieName = "grocery"
                      , sc_sessionTTL = 86000 -- check if seconds
                      , sc_sessionIdEntropy = 10
                      , sc_emptySession = 0
                      , sc_persistCfg = Nothing
                      }

    -- No App State we care about
    appState = ()

routes :: DB.ConnectionPool -> SpockM DB.SqlBackend sess () ()
routes pool = do
  Login.routes pool

applyMiddleware :: Application -> Application
applyMiddleware = logStdoutDev . (gzip def) . staticServing
  where
    staticServing = staticPolicy (noDots <> addBase "static")

