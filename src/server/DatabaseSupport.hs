{-# LANGUAGE OverloadedStrings          #-}

module DatabaseSupport where

import Control.Monad.Logger (runStdoutLoggingT)
import Control.Monad.Trans (liftIO)
import qualified Database.Persist as DB
import qualified Database.Persist.Sqlite as DB

getPool :: IO DB.ConnectionPool
getPool = do
  let s = "ghci.db"
  let n = 1
  runStdoutLoggingT (DB.createSqlitePool s n)

runDB :: DB.ConnectionPool -> DB.SqlPersistT IO a -> IO a
runDB pool query = liftIO $ DB.runSqlPool query pool

